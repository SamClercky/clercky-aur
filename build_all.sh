#!/bin/bash

echo "[*] Building all packages"
for pkg in $(ls -d x86_64/*/)
do
    echo "[**] Building $pkg"
    ./build_pkg.sh $pkg
done

pushd x86_64
echo "[*] Creating db"
repo-add clercky-aur.db.tar.gz *.pkg.tar.zst

echo "[*] fixing problems with symlinks"
mv clercky-aur.db.tar.gz clercky-aur.db
mv clercky-aur.files.tar.gz clercky-aur.files

echo "Clean old"
rm *.old
