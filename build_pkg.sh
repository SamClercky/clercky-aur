#!/bin/bash

PKG=$1
echo "building $1"

pushd $PKG
echo "Going into pkg dir: $PKG"

echo "Building pkg"
makepkg

echo "Checking PKGBUILD"
namcap PKGBUILD

echo "Checking pkg"
namcap *.pkg.tar.zst

echo "Move into place"
mv *.pkg.tar.zst ..
